#!/bin/bash

#############################################################
#
# Description : Déploiement de conteneur docker
# Auteur : Aymen BEN MESSAOUD (par XAVKI)
# Date : 13/04/2019
#############################################################

# si option create
if [ "$1" == "--create" ]; then

  # récupération de nombre de conteneur
  nb_machine=1
  [ "$2" != "" ] && nb_machine=$2

  # Setting min/Max
  min=1
  max=0
  # Récupération de l'id Max
  idmax=`docker ps -a --format '{{ .Names }}' | awk -F "-" -v user=$USER '$0 ~ user"-alpine" {print $3}' | sort -r | head -1`

  min=$(($idmax + 1))
  max=$(($idmax + $nb_machine))

  # Création conteneurs docker
  echo "Début de la création des conteneurs"
  for i in $(seq $min $max); do
    docker run -tid --name $USER-alpine-$i alpine:latest
    echo "Conteneur $USER-alpine-$i créé"
  done

# si option drop
elif [ "$1" == "--drop" ]; then
  # Suppression des conteneurs
  echo ""
  echo "Suppression des conteneurs..."
  echo ""
  docker rm -f $(docker ps -a | grep $USER-alpine | awk '{print $1}')
  echo ""

# si option Start
elif [ "$1" == "--start" ]; then
  # start d'un conteneur docker
  echo ""
  echo "Démarrage des conteneurs..."
  echo ""
  docker start $(docker ps -a | grep $USER-alpine | awk '{print $1}')
  echo ""

# si option info
elif [ "$1" == "--info" ]; then
  # status des conteneur
  echo ""
  echo "Informations des conteneurs :"
  echo ""
  for conteneur in $(docker ps -a | grep $USER-alpine | awk '{print $1}');do
    docker inspect -f '  => {{ .Name }} - {{ .NetworkSettings.IPAddress }}' $conteneur
  done
    echo ""

# si option info
elif [ "$1" == "--ansible" ]; then
    echo ""
    echo "Notre option est --ansible"
    echo ""

else
echo "

Options :

  - --create : lancer des conteneurs
  - --drop : supprimer les conteneurs créé par deploy.sh
  - --info : caractéristiques des conteneurs (nom, ip, user ...)
  - --start : redémarrage des conteneurs
  - --ansible : déploiement arborescence ansible

"
fi
